export default {
    testimonials:[
        {
            image: 'images/client.png',
            credentials: 'Cengiz Onkal, Tech Lead, Cyprus International University',
            quote: 'Brian has been a fantastic addition to our software dev team. He assimilated easily into our team. His enthusiasm and skills for his work and his superb.'
        },{
            image: 'images/client.png',
            credentials: 'Tymon Gozhore, CEO, Innanetwave.',
            quote: 'Another testimonial'
        },
    ]
}
