export default {
    skillSet: [
        {
            title: 'Professional',
            skills: [
                {
                    name: 'React, React Native, ReDUX',
                    confidence: '90%'
                }, {
                    name: 'Flutter',
                    confidence: '70%'
                }, {
                    name: 'Vue JS',
                    confidence: '80%'
                }, {
                    name: 'Laravel/PHP/MySQL',
                    confidence: '60%'
                }, {
                    name: 'git VCS',
                    confidence: '80%'
                }, {
                    name: 'Android/Java',
                    confidence: '70%'
                }, {
                    name: 'iOS/Swift',
                    confidence: '60%'
                }
            ]
        }, {
            title: 'Software',
            skills: [
                {
                    name: 'Linux OS',
                    confidence: '90%'
                }, {
                    name: 'Mac OS',
                    confidence: '80%'
                }, {
                    name: 'Windows',
                    confidence: '50%'
                }, {
                    name: 'Jetbrains IDEs',
                    confidence: '80%'
                }, {
                    name: 'Sublime Text',
                    confidence: '80%'
                }
            ]
        },{
            title: 'Personal',
            skills: [
                {
                    name: 'Dedication',
                    confidence: '90%'
                }, {
                    name: 'Teamwork',
                    confidence: '90%'
                }, {
                    name: 'Creativity',
                    confidence: '90%'
                }, {
                    name: 'Communication',
                    confidence: '90%'
                }
            ]
        },
    ]
}
