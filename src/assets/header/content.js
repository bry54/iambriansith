export default {
    credentials: {
        profileImage: require('@/assets/header/my-profile.png'),//'my-profile.png',
        fullName: 'Brian P. Sithole',
        role: 'Web & Mobile Applications Developer'
    },
    contacts: [
        {
            icon: 'las la-envelope',
            value: 'brianacyth@icloud.com'
        },
        {
            icon: 'lab la-chrome',
            value: 'www.iambriansith.com'
        },
        {
            icon: 'lab la-skype',
            value: 'brianacyth'
        },
        {
            icon: 'las la-map-marker',
            value: 'N. Cyprus | Turkey'
        },
    ],
    profiles: [
        {
            icon: 'lab la-linkedin',
            url: 'https://cy.linkedin.com/in/brian-paidamoyo-sithole-b6544877',
            color: 'blue darken-3'
        },
        {
            icon: 'lab la-bitbucket',
            url: 'https://bitbucket.org/bry54/',
            color: 'blue darken-4'
        },
        {
            icon: 'lab la-twitter',
            url: 'https://twitter.com/brizzy_p',
            color: 'blue'
        },
        {
            icon: 'lab la-facebook-f',
            url: 'https://www.facebook.com/brian.p.sithole',
            color: 'indigo'
        },
        {
            icon: 'lab la-instagram',
            url: 'https://www.instagram.com/brianacyth/',
            color: 'purple'
        }
    ]
}
