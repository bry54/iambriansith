export default {
    experiences:[
        {
            jobTitle: 'Full stack Developer',
            companyName: 'Cyprus International University',
            workPeriod: '2017 - Present',
            summary: 'Frontend & backend developer for Cyprus International University corporate applications.',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }, {
            jobTitle: 'Mobile Application Developer',
            companyName: 'Cyprus International University',
            workPeriod: '2015 - 2017',
            summary: 'Specialised in mobile application development using native platform languages JAVA for android and Swift for IOS',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }, {
            jobTitle: 'Web Developer (Voluntary Internship)',
            companyName: 'Innovia Digital',
            workPeriod: 'July 2014 - September 2014',
            summary: 'Developing mockup websites using Wordpress',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }, {
            jobTitle: 'Web Developer (Internship)',
            companyName: 'Innovia Digital',
            workPeriod: 'July 2013 - September 2013',
            summary: 'Designing mockup websites using Sencha Touch',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }
    ]
}
