export default {
    title: 'Interests',
    intro:'I love coding, hobby programming and learning new technologies is something I can do to wind-down or just relax. Software Development is a fast paced industry, so to avoid burnout I have other activities',
    interests: [
        {
            name: 'Chess',
            icon: 'las la-chess-knight'
        }, {
            name: 'Music',
            icon: 'las la-music'
        }, {
            name: 'Fitness',
            icon: 'las la-dumbbell'
        }, {
            name: 'Gaming',
            icon: 'las la-gamepad'
        }, {
            name: 'Reading',
            icon: 'las la-book-reader'
        }, {
            name: 'Cricket',
            icon: 'las la-baseball-ball'
        }
    ]
}
