export default {
    educations:[
        {
            educationLevel: 'MSc. Computer Engineering',
            institutionName: 'Cyprus International University',
            studyPeriod: null/*'2017 - Present'*/,
            headlight: 'THESIS TOPIC',
            summary: 'Implementation of secure mobile payment system',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }, {
            educationLevel: 'BSc. Information Systems Engineering',
            institutionName: 'Cyprus International University',
            studyPeriod: null/*'2011 - 2015'*/,
            headlight: 'GRADUATION PROJECT',
            summary: 'Design and implementation of University Mobile Application on Android platform.',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }, {
            educationLevel: 'Diploma In Ecommerce & Web Design',
            institutionName: 'McMaine School Of Computing',
            studyPeriod: null/*'2010'*/,
            headlight: 'EXAMINING BOARD',
            summary: 'Business and Computing Examinations (B.C.E), London (U.K)',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }, {
            educationLevel: 'GCE \'A\' Level',
            institutionName: 'Chemhanza High School (Zimbabwe)',
            studyPeriod: null/*'2004-2006'*/,
            headlight: 'SUBJECTS',
            summary: 'Mathematics, Computing, Management Of Business',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }, {
            educationLevel: 'GCSE \'O\' Level',
            institutionName: 'Chemhanza Secondary School (Zimbabwe)',
            studyPeriod: null/*'2001-2004'*/,
            headlight: 'SUBJECTS',
            summary: '10 \'O\' Level passes including Mathematics and English',
            body: {
                img: null,
                description:{
                    text: 'Full text of experience',
                    achievements:[
                        {
                            name: 'Achievement Title',
                            desc: 'Achievement Description',
                            url: 'www.something.com'
                        }
                    ]
                }
            }
        }
    ]
};
