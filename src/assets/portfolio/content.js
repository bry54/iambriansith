/**
 * @property tabs: Array
 */
export default{
    tabs: [
        {
            title: 'Mobile App Projects',
            target: 'tab-mobile',
            showcaseItems: [
                {
                    image: 'images/portfolio.png',
                    title: 'Fly Mango App Prototype',
                    type: 'Freelance Project',
                    stack: 'React Native, ReDUX persist, React Navigation,Expo',
                    description: 'Some Description 1.1',
                    url: 'https://expo.io/@bry54/flymango'
                },{
                    image: 'images/portfolio.png',
                    title: 'CIU Mobile',
                    type: 'Corporate Project',
                    stack: 'React Native, ReDUX persist, React Navigation, Firebase',
                    description: 'Some Description 1.1',
                    url: 'https://www.ciu.edu.tr/page/ciu-mobile-2848'
                }, {
                    image: 'images/portfolio.png',
                    title: 'CIU Equipment Ctrl Sys',
                    type: 'Corporate Project',
                    stack: 'Native Android (JAVA)',
                    description: 'Some Description 1.2',
                    url: null
                }, {
                    image: 'images/portfolio.png',
                    title: 'CIUMobile (Android)',
                    type: 'Corporate Project',
                    stack: 'Native Android (JAVA)',
                    description: 'Some Description 1.2',
                    url: 'https://play.google.com/store/apps/details?id=tr.edu.ciu.ciumobile'
                }, {
                    image: 'images/portfolio.png',
                    title: 'CIUMobile (ios)',
                    type: 'Corporate Project',
                    stack: 'Native iOS (xcode/Swift)',
                    description: 'Some Description 1.2',
                    url: 'https://apps.apple.com/us/app/ciu-mobile/id1213507451'
                }
            ]
        }, {
            title: 'Web App Projects',
            target: 'tab-web',
            showcaseItems: [
                {
                    image: 'images/portfolio.png',
                    title: 'iambriansith',
                    type: 'Personal Website',
                    stack: 'Vue, MongoDB',
                    description: 'Some Description 2.1',
                    url: 'http://206.189.23.170/'
                },{
                    image: 'images/portfolio.png',
                    title: 'CIU Mobile API & Admin',
                    type: 'Corporate Project',
                    stack: 'Laravel/PHP, Vue, MySQL',
                    description: 'Some Description 2.1',
                    url: null
                }, {
                    image: 'images/portfolio.png',
                    title: 'CIU Website',
                    type: 'Corporate Project (Contributor)',
                    stack: 'Laravel/PHP',
                    description: 'Some Description 2.1',
                    url: 'https://www.ciu.edu.tr'
                }, {
                    image: 'images/portfolio.png',
                    title: 'Afrikwanza Website',
                    type: 'Freelance Project',
                    stack: 'HTML5, JS, CSS, Bootstrap',
                    description: 'Some Description 2.2',
                    url: 'http://afrikwanza.co.za/'
                }, {
                    image: 'images/portfolio.png',
                    title: 'CIU Equipment Ctrl Sys',
                    type: 'Corporate Project',
                    stack: 'Laravel/PHP, Vue',
                    description: 'Some Description 2.2',
                    url: null
                }, {
                    image: 'images/portfolio.png',
                    title: 'CIU Personnel Reporting Sys',
                    type: "Corporate Project",
                    stack: 'Laravel/PHP',
                    description: 'Some Description 2.2',
                    url: null
                }
            ]
        }, {
            title: 'E-commerce Projects',
            target: 'tab-ecommerce',
            showcaseItems: [

            ]
        }
    ]
}
